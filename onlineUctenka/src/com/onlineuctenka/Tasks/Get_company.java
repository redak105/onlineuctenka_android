package com.onlineuctenka.Tasks;



import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.onlineuctenka.R;
import com.onlineuctenka.classes.Company;
import com.onlineuctenka.classes.User;
import com.onlineuctenka.php.Database;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


/**
 * Class to load list of categories
 *
 * @author redak105
 *
 */
public class Get_company extends AsyncTask<Void, Void, Boolean> {
	
	/** Context from with we call this task */
	private Context context = null;
	/** Returning object from task */
	private JSONObject ret = null;
	/** Login user */
	private User user = null;
	
	/** Dialog to show loading */
	private ProgressDialog dialog_prog;
	
	/**
	 * Constructor
	 * @param context current context
	 * @param user class of login user
	 */
	public Get_company(Context context, User user) {
		this.context = context;
		this.user = user;
		
		this.dialog_prog = new ProgressDialog(this.context);
    }
	
	protected void onPreExecute(){ 
		super.onPreExecute();
		   
		// show load.. dialog
		this.dialog_prog.setMessage(context.getResources().getString(R.string.task_company_get));
		this.dialog_prog.setCancelable(false);
		this.dialog_prog.show();
	}
	
	@Override
    protected Boolean doInBackground(Void... params) {
		// connector to database
    	Database db = Database.getInstance();
		
    	// create parameters list
		List<NameValuePair> values = new ArrayList<NameValuePair>(2);
		values.add( new BasicNameValuePair("submit", "get_comp"));
		values.add( new BasicNameValuePair("id_user", String.valueOf(user.getId())));
		
		// send data
		this.ret = db.post(values);
		
		// test of success sending
		int succes;
		try {
			succes = this.ret.getInt("success");
			if (succes == 1)
			{
				return true;
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
//			AlertDialog.Builder builder = new AlertDialog.Builder(activity);  
//    		builder.setTitle(activity.getResources().getString(R.string.task_company_error));
//            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {  
//                 @Override  
//                 public void onClick(DialogInterface dialog, int which) {  
//                      dialog.cancel();
//                 }  
//            });  
//            AlertDialog alert = builder.create();  
//            alert.show();
		}
					
		return false;
    }


    protected void onPostExecute(Boolean succes) {
    	dialog_prog.dismiss();
    	
    	// test to validate success of task 
    	if (succes)
    	{
    		try {
    			// parse return data
				parse_comp(ret);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
        
    }
    
    /**
     * Parse data to list of companies
     * @param ret return object from task to DB
     * @throws JSONException
     */
    private void parse_comp(JSONObject ret) throws JSONException
	{
    	// create new array list of companies
		ArrayList<Company> list = new ArrayList<Company>();
        	
		// get array from object
		JSONArray array = this.ret.getJSONArray("companyies");
		
		// search array
		for (int i=0; i<array.length(); i++)
		{
			// get object from array
			JSONObject obj = array.getJSONObject(i);
			
			// get parameters from object
			int id = obj.getInt("id");
			String name = obj.getString("name");
			
			// create new company
			Company comp = new Company(id, name);
			
			// add category to list
			list.add(comp);
		}
		
		// find sinner in context 
		Spinner spinner_comp = (Spinner) ((Activity) this.context).findViewById(R.bill_save.company_spinner);
		
		// fill spinner with parsed data
		ArrayAdapter<Company> company_adapter = new ArrayAdapter<Company>(context, android.R.layout.simple_spinner_item, list);
		company_adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
		spinner_comp.setAdapter(company_adapter);		
	}
}