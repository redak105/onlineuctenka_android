package com.onlineuctenka.Tasks;



import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import com.onlineuctenka.R;
import com.onlineuctenka.classes.Category;
import com.onlineuctenka.classes.Company;
import com.onlineuctenka.classes.User;
import com.onlineuctenka.php.Database;
import com.onlineuctenka.settings.Settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;


/**
 * Class to load list of categories
 *
 * @author redak105
 *
 */
public class Save_bill extends AsyncTask<Void, Void, Boolean> {
	
	/** Context from with we call this task */
	private Activity activity = null;
	/** Returning object from task */
	private JSONObject ret = null;
	/** Login user */
	private User user = null;
	
	/** Dialog to show loading */
	private ProgressDialog dialog_prog;
	
	/** String to save error messages */
	String error = null;
	
	/**
	 * Constructor
	 * @param activity current activity
	 * @param user class of login user
	 */
	public Save_bill(Activity activity, User user) {
		this.activity = activity;
		this.user = user;
		
		this.dialog_prog = new ProgressDialog(this.activity);
    }
	
	protected void onPreExecute(){ 
		super.onPreExecute();

		// show load.. dialog
	   this.dialog_prog.setMessage(this.activity.getResources().getString(R.string.task_save));
	   this.dialog_prog.setCancelable(false);
	   this.dialog_prog.show(); 
	}
	
    protected Boolean doInBackground(Void... params) {
    	// reset error messages
    	this.error = "";
    	
    	// connector to database
    	Database db = Database.getInstance();
		
    	// load widgets from activity
    	EditText d_name = (EditText)  this.activity.findViewById(R.bill_save.name_text);
    	Spinner d_company = (Spinner) this.activity.findViewById(R.bill_save.company_spinner);
    	
    	EditText d_date = (EditText) this.activity.findViewById(R.bill_save.date_text);
    	EditText d_assurance = (EditText) this.activity.findViewById(R.bill_save.assurance_text);
    	EditText d_description = (EditText) this.activity.findViewById(R.bill_save.description_text);
    	
    	// get and test if is set name
    	String name = d_name.getText().toString();
    	if (name.length() <= 0)
    	{
    		this.error += this.activity.getResources().getString(R.string.task_save_name);
    	}

    	// get and test if is set company
    	String company = "-1";
    	if (d_company.getCount() > 0)
    	{
    		company = Integer.toString(((Company) d_company.getSelectedItem()).getId());
    	}
    	else {
    		this.error += this.activity.getResources().getString(R.string.task_save_company);	    		
    	}
    	
    	// get and test if is set category
    	String category = "-1";
    	Category selected = null;
    	if (Get_category.is_instance())
    	{
    		Get_category cat = Get_category.getInstance(null);
    		selected = cat.get_selected();
    	}
    	if (selected != null)
    	{
    		category = Integer.toString(selected.getId());
    	}
    	else {
    		this.error += this.activity.getResources().getString(R.string.task_save_category);	    
    	}
    	
    	// get and test if is set date
    	String date = d_date.getText().toString();
    	if (date.length() <= 0)
    	{
    		this.error += this.activity.getResources().getString(R.string.task_save_date);
    	}
    	
    	// get and test if is set assurance
    	String assurance = d_assurance.getText().toString();
    	if  (assurance.length() <= 0)
    	{
    		this.error += this.activity.getResources().getString(R.string.task_save_assurance);
    	}
    	
    	// get description
    	String description = d_description.getText().toString();
    	
    	// get and test image
    	ImageView image = (ImageView) this.activity.findViewById(R.bill_save.image_view);
    	Bitmap bitm = null;
    	BitmapDrawable bitmap_draw = (BitmapDrawable) image.getDrawable();
    	if (bitmap_draw != null)
    	{
    		bitm = bitmap_draw.getBitmap();
    	}
    	else {
    		this.error += this.activity.getResources().getString(R.string.task_save_image);
		}	    	
    	
    	// test if there is error
    	if (this.error.length() >0)
    	{
    		return false;
    	}
    	
		// **resize bitmap**
		// minimal size of bitmap
		float min = Settings.max_size;
		
		// get size
		int width = bitm.getWidth();
		int height = bitm.getHeight();
		Log.i("size", "width " + width + "height" + height);
		
		// get new size
		if (width > min)
		{
			float scale = min/width;
			width = (int) min;
			height = (int) (height*scale);
		}
		if (height > min)
		{
			float scale = min/height;
			height = (int) min;
			width = (int) (width*scale);
		}
		
		Log.i("resize", "width " + width + "height" + height);
		// resize of bitmap
		Bitmap bitmap = Bitmap.createScaledBitmap(bitm, width, height, true);
		bitm.recycle();
		
		// create output stream to compress bitmap
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		// compress bitmap
		bitmap.compress(Bitmap.CompressFormat.JPEG, Settings.jpeg_compress, bao);
		// convert bitmap
		byte[] ba = bao.toByteArray();
		
		// create parameters list
		MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		
		try {
			// set parameters to list
			FormBodyPart bodyPart;
			bodyPart = new FormBodyPart("submit", new StringBody("save"));
		
			reqEntity.addPart(bodyPart);
			bodyPart = new FormBodyPart("id_user", new StringBody(String.valueOf(user.getId())));
			reqEntity.addPart(bodyPart);
			bodyPart = new FormBodyPart("name", new StringBody(name));
			reqEntity.addPart(bodyPart);
			bodyPart = new FormBodyPart("company", new StringBody(company));
			reqEntity.addPart(bodyPart);
			bodyPart = new FormBodyPart("category", new StringBody(category));
			reqEntity.addPart(bodyPart);
			bodyPart = new FormBodyPart("date", new StringBody(date));
			reqEntity.addPart(bodyPart);
			bodyPart = new FormBodyPart("assurance", new StringBody(assurance));
			reqEntity.addPart(bodyPart);
			bodyPart = new FormBodyPart("description", new StringBody(description));
			reqEntity.addPart(bodyPart);
			
			ByteArrayBody bab = new ByteArrayBody(ba, "image/jpeg", Settings.name_of_send_image);
			reqEntity.addPart("file", bab);
			
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// send data
		this.ret = db.post(reqEntity);
		
		// test of success sending
		int succes;
		try {
			succes = this.ret.getInt("success");
			if (succes == 1)
			{
				return true;
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
					
		return false;
    }


    protected void onPostExecute(Boolean succes) {
    	this.dialog_prog.dismiss();
    	
    	// test to validate success of task 
    	if (succes)
    	{
    		// show dialog with action success
    		AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);  
    		builder.setTitle(this.activity.getResources().getString(R.string.task_save_OK));
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {  
                 @Override  
                 public void onClick(DialogInterface dialog, int which) {  
                      dialog.cancel();
                      activity.finish();
                      
                 }  
            });  
            AlertDialog alert = builder.create();  
            alert.show();	    		
    	}
    	else {
    		// show dialog with errors success
    		AlertDialog.Builder builder = new AlertDialog.Builder(activity);  
    		builder.setTitle(this.activity.getResources().getString(R.string.task_save_error));
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {  
                 @Override  
                 public void onClick(DialogInterface dialog, int which) {  
                      dialog.cancel();  
                 }  
            });  
    		
    		try {
    			// test if errors occurred in server
    			if (this.ret != null)
				{
    				// load error message and show dialog
    				String mess = this.ret.getString("message");
    				builder.setMessage(mess);
    				AlertDialog alert = builder.create();  
		            alert.show();
		            
		            return;
				}
    			// test if errors occurred in client side
    			if (this.error.length() >= 0)
				{
    				// load error message and show dialog
    				builder.setMessage(this.error);
    				AlertDialog alert = builder.create();  
		            alert.show();
		            return;
				}
	    		
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
}