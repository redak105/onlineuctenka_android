package com.onlineuctenka.php;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import com.onlineuctenka.settings.Settings;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Class to connect to database. Sends and receiving posts.
 * 
 * @author redak105
 *
 */
public class Database 
{
	/** instance of simpleton */
	private static Database instance = new Database();
	/** manger to connect to PHP server */
    ConnectivityManager connectivity_manager;
    /** information about connection */
    NetworkInfo wifiInfo, mobile_nfo;
    
    /**
     * Get instance of simpleton
     * @return class of Database
     */
    public static Database getInstance() {
        return instance;
    }
    
    /**
     * Test if device is connected to Internet
     * @param con current context
     * @return state of connection
     */
    public static boolean isOnline(Context con) {
    	Boolean connected = false;
    	try {
        	// load connectivity
        	ConnectivityManager connectivity_manager = (ConnectivityManager) con.getSystemService(Context.CONNECTIVITY_SERVICE);
        	// get info
            NetworkInfo networkInfo = connectivity_manager.getActiveNetworkInfo();
            if ((networkInfo != null) && networkInfo.isAvailable() && networkInfo.isConnected())
            {
            	connected = true; 
            }
        } catch (Exception e) {
            Log.v("connectivity", e.toString());
        }
        return connected;
    }
	
	/** input stream from server */
	private static InputStream input = null;
	/** JSONObject to save result */
	private static JSONObject json_obj = null;
	/** String from json_oject */
	private static String json = "";

	public Database() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Send POST to PHP servers with parameters
	 * @param params parameter to POST
	 * @return object from server
	 */
	public JSONObject post(List<NameValuePair> params)
	{
        try {
            // default http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            // http post
            HttpPost httpPost = new HttpPost(Settings.url);
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			// responce from server
			HttpResponse httpResponse = httpClient.execute(httpPost);
	        HttpEntity httpEntity = httpResponse.getEntity();
	        input = httpEntity.getContent();
	        
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	// buffer to read from server 
            BufferedReader reader = new BufferedReader(new InputStreamReader(input, "iso-8859-1"), 8);
            // save from buffer to String
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            // close stream
            input.close();
            // parse to string
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        
        Log.i("pasrser", "json " + json);
        
        try {
        	// parse json object
            json_obj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
 
        // return JSON String
        return json_obj;
	}
	
	public JSONObject post(MultipartEntity params)
	{
        try {
        	// default http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            // http post
            HttpPost httpPost = new HttpPost(Settings.url);
            httpPost.setEntity(params);
			// responce from server
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
	        input = httpEntity.getContent();
	        
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	// buffer to read from server 
            BufferedReader reader = new BufferedReader(new InputStreamReader(input, "iso-8859-1"), 8);
            // save from buffer to String
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            // close stream
            input.close();
            // parse to string
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        
        Log.i("pasrser", "json " + json);
        
        try {
        	// parse json object
            json_obj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
 
        // return JSON String
        return json_obj;
	}
}
