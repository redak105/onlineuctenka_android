package com.onlineuctenka;

import com.onlineuctenka.classes.Session_manager;
import com.onlineuctenka.classes.User;
import com.onlineuctenka.php.Database;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	/** class of login user */
	User user = null;	
	/** class of session manager */
	Session_manager session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		this.session = new Session_manager(MainActivity.this);
        this.session.check();
        
        this.user = session.get_user();
        
        TextView text = (TextView) findViewById(R.list.user_name);
        text.setText(this.user.getEmail());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void onStart() 
	{
		super.onStart();
		
		this.session = new Session_manager(MainActivity.this);
		if (!this.session.check())
		{
			this.finish();
		}
	}
	
	public void onResume() 
	{
		super.onResume();
		
		this.session = new Session_manager(MainActivity.this);
		if (!this.session.check())
		{
			this.finish();
		}
		
		Boolean online = Database.isOnline(MainActivity.this);
        if (!online)
        {
			show_error();
			return;
        }
	}
	
	/**
	 * Show error message for not connection
	 */
	private void show_error()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);  
		builder.setTitle("You are not connect to internet");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {  
             @Override  
             public void onClick(DialogInterface dialog, int which) {  
                  dialog.cancel();
             }  
        });  
        AlertDialog alert = builder.create();  
        alert.show();
	}
	
	/**
	 * Create activity to add new bill
	 * @param view
	 */
	public void add(View view)
	{
		Boolean online = Database.isOnline(MainActivity.this);
        if (!online)
        {
			show_error();
			return;
        }
		
		Uri uri = Uri.parse("onlineUctenka://add");
        Bundle bundle = new Bundle();
        
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        
        intent.putExtras(bundle);
        startActivity(intent);
	}
	
	/**
	 * onClick action to logout user
	 * @param view
	 */
	public void logout(View view)
	{
		this.session = new Session_manager(MainActivity.this);
		this.session.logout();
		
		this.finish();
	}

	
	public boolean onOptionsItemSelected(MenuItem item) {
	       
        switch (item.getItemId()) {
        	//delete
        	case R.menu_main.quit: {
        		// Closing all the Activities
        		finish();          
                moveTaskToBack(true);
        		return true;
        	}      	
        }
        return false;
    }
}
