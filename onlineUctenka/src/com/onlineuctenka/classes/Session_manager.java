package com.onlineuctenka.classes;

import com.onlineuctenka.LoginActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Class to manage user session
 * 
 * @author redak105
 *
 */
public class Session_manager 
{
	/** Shared Preferences */
    private SharedPreferences pref;
 
    /** Editor for Shared preferences  */
    private Editor editor;
 
    /** Context */
    private Context context;
 
    /** Shared preferences mode */
    private int PRIVATE_MODE = 0;
 
    /** Shared preferences file name */
    private static final String PREF_NAME = "online-uctenka";
    
    /** User name (make variable public to access from outside) */
    public static final String KEY_ID = "id";
 
    /** Email address (make variable public to access from outside) */
    public static final String KEY_EMAIL = "email";
 
    /**
     * Constructor
     * @param context {@link Context} 
     */
    public Session_manager(Context context){
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
 
    /**
     * Create login session
     * @param id id of user
     * @param email email of user
     */
    public void create(int id, String email){ 
        // Storing name in pref
        editor.putInt(KEY_ID, id);
 
        // Storing email in pref
        editor.putString(KEY_EMAIL, email);
 
        // commit changes
        editor.commit();
    }  
    
    /**
     * Check login method will check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * @return {@link Boolean} if session is initialized
     */
    public Boolean check(){
        // Check login status
        
    	if (pref.contains(KEY_ID))
    	{
	    	if (pref.getInt(KEY_ID, 0) > 0)
	        {
	            return true;
	        }
	    	// user is not logged in redirect him to Login Activity
            Intent i = new Intent(context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
 
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
 
            // Staring Login Activity
            context.startActivity(i);
            return false;
    	}
    	return false;
    }
    
    /**
     * Get stored session data
     * @return User class
     */
    public User get_user()
    {
    	User user = new User(pref.getInt(KEY_ID, -1), pref.getString(KEY_EMAIL, null));
    	
        return user;
    }
    
    /**
     * Clear session details 
     */
    public void logout(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
 
        // After logout redirect user to Loing Activity
        Intent i = new Intent(context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
 
        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
 
        // Staring Login Activity
        context.startActivity(i);
    }
	
    @SuppressWarnings("deprecation")
	public static void quit()
    {
    	//Process.killProcess(Process.myPid());
    	//System.runFinalizersOnExit(true);
    	
		System.exit(0);
    }
}
