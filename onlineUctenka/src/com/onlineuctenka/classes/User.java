package com.onlineuctenka.classes;

/**
 * Class to save information about user
 * 
 * @author redak105
 *
 */
public class User {

	
	/** id of user */
	private int id;
	/** email of user */
	private String email;
	
	/**
	 * Constructor
	 * @param id id of new user
	 * @param email email of new user
	 */
	public User(int id, String email) {
		this.id = id;
		this.email = email;
	}

	/**
	 * Get id of user
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Get email of user
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
		
}
